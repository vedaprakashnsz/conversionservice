package com.trezi.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * @author Mourya Balla
 * @project converter
 * @CreatedOn 19-06-2019
 */
public class ConversionJobDeserializer implements Deserializer<ConversionJob> {

    /**
     * Configure this class.
     *
     * @param configs configs in key/value pairs
     * @param isKey   whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    /**
     * Deserialize a record value from a byte array into a value or object.
     *
     * @param topic topic associated with the data
     * @param data  serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @Override
    public ConversionJob deserialize(String topic, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        ConversionJob object = null;
        try {
            object = mapper.readValue(data, ConversionJob.class);
        } catch (Exception exception) {
            System.out.println("Error in deserializing bytes "+ exception);
        }
        return object;
    }

    @Override
    public void close() {

    }


}
