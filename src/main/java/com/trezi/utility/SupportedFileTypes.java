package com.trezi.utility;

/**
 * @author Mourya Balla
 * @project converter
 * @CreatedOn 18-06-2019
 */
public enum SupportedFileTypes {
    RVT,SKP
}
