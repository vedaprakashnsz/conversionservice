package com.trezi.utility;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Service;

/**
 * @author Mourya Balla
 * @project cataloglibrary
 * @CreatedOn 19-06-2019
 */
@Getter
@Setter
@ToString
public class ConversionJob {
    String fileType;
    String url;
}
