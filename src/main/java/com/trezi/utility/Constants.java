package com.trezi.utility;

public class Constants {

    public static final String RESPONSE_SUCCESS_STATUS = "success";
    public static final String RESPONSE_FAILED_STATUS = "failure";
    public static final Integer REQUEST_FAILURE_CODE = 500;
    public static final Integer REQUEST_SUCCESS_CODE = 200;
    public static final Integer UNAUTHORIZED_ACCESS_CODE = 401;
    public static final String OWNED = "owned";
    public static final String SHARED = "shared";
    public static final String REVIT_INSTALLATION = "C:\\Program Files\\Notepad++\\notepad++.exe";
    public static final String SKETCH_UP_CONVERTER_EXE = "C:\\Users\\RevitVM0\\Desktop\\SkpToTZIConverter\\App_ConvertToTZI.exe";
    public static final String REVIT_CONVERTER_EXE = "C:\\Program Files\\Autodesk\\Revit 2019\\Revit.exe";
}