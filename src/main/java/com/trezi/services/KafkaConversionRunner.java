package com.trezi.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Mourya Balla
 * @project converter
 * @CreatedOn 19-06-2019
 */
@Component
@Slf4j
public class KafkaConversionRunner {

    private final KafkaConvertorService kafkaConvertorService;

    private ExecutorService executorService;

    public KafkaConversionRunner(KafkaConvertorService kafkaConvertorService) {
        this.kafkaConvertorService = kafkaConvertorService;
    }

    /**
     * Callback used to run the bean.
     *
     * @throws Exception on error
     */
    @PostConstruct
    public void run() throws Exception {
        BasicThreadFactory factory = new BasicThreadFactory.Builder()
                .namingPattern("kafka-thread-%d").build();
        executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            try {
                kafkaConvertorService.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    @PreDestroy
    public void beandestroy() {
        if(executorService != null){
            executorService.shutdownNow();
        }
    }
}
