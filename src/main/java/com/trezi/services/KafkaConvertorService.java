package com.trezi.services;

import com.google.common.io.Resources;
import com.google.common.util.concurrent.*;
import com.trezi.utility.ConversionJob;
import com.trezi.utility.SupportedFileTypes;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.Executors;

/**
 * @author Mourya Balla
 * @project converter
 * @CreatedOn 18-06-2019
 */
@Slf4j
@Service
public class KafkaConvertorService {
    static KafkaConsumer<String, ConversionJob> consumer = null;
    private static ListeningExecutorService skpTaskExecutor =
            MoreExecutors.listeningDecorator(Executors
                    .newFixedThreadPool(2,new ThreadFactoryBuilder().setNameFormat("skp-conv-thread-%d").build()));

    private static ListeningExecutorService rvtTaskExecutor =
            MoreExecutors.listeningDecorator(Executors
                    .newFixedThreadPool(1,new ThreadFactoryBuilder().setNameFormat("rvt-conv-thread-%d").build()));


    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }

    @Async
    public static void init() throws IOException {
        createKafkaConsumerOnTopic("convertor-ms");
        consumePartition();
    }

    private static void createKafkaConsumerOnTopic(String topicName) throws IOException {
        String consumerGroupId;
        InputStream props = Resources.getResource("kafkaconsumer.properties").openStream();
        try {
            Properties properties = new Properties();
            properties.load(props);
            consumerGroupId = "CG-SYS-CONVERTOR-MS";
            properties.setProperty("group.id", consumerGroupId);
            properties.put("value.deserializer", com.trezi.utility.ConversionJobDeserializer.class);
            properties.put("enable.auto.commit",false);
            consumer = new KafkaConsumer<>(properties);
            //cyclicBarrier = new CyclicBarrier(properties.getProperty());
            /*TopicPartition partition0 = new TopicPartition(topicName, 0);
            consumer.assign(Arrays.asList(partition0));*/
            consumer.subscribe(Collections.singleton(topicName));



        } finally {
            props.close();
        }
        log.info("created Kafka Consumer for updating model file on topic {} with group id {}",topicName,consumerGroupId);
    }

    private static void consumePartition() {
        int count = 0;

        try {
            List<ListenableFuture<?>> futures = new ArrayList<>();

            while (true) {

                // Poll for records

                ConsumerRecords<String, ConversionJob> records = consumer.poll(1000);

                //if(futures.size() > 0) futures.clear();
                //taskExecutor.awaitTermination()

                log.info("Number of jobs fetched:"+records.count());

                for (ConsumerRecord<String, ConversionJob> record : records) {

                    try {
                        count += 1;
                        log.info("record.value():" + record.value());
                        log.info("count:" + count);

                    /*JsonParser jsonParser = new JacksonJsonParser();
                    Map conversionJobMetadata = jsonParser.parseMap(record.value());
                    ListenableFuture<Map<String, String>> listenableFuture = taskExecutor.submit(new ConversionServiceAction(SupportedFileTypes.valueOf((String)conversionJobMetadata.get("type")), (String)conversionJobMetadata.get("url")));*/
                        ConversionJob job = record.value();
                        ListenableFuture<Map<String, String>> listenableFuture =null;
                        if(SupportedFileTypes.valueOf(job.getFileType()).equals(SupportedFileTypes.SKP)){
                            listenableFuture = skpTaskExecutor.submit(new ConversionServiceAction(SupportedFileTypes.valueOf(job.getFileType()), job.getUrl()));
                        }else if(SupportedFileTypes.valueOf(job.getFileType()).equals(SupportedFileTypes.RVT)){
                            listenableFuture = rvtTaskExecutor.submit(new ConversionServiceAction(SupportedFileTypes.valueOf(job.getFileType()), job.getUrl()));
                        }

                        Futures.addCallback(listenableFuture, new ConversionJobCallback());
                        futures.add(listenableFuture);
                        log.info("%%%%%%%%%%%%%%%%%%%");
                    }catch (Exception e){
                        log.info("Error handling job..continuing...",e);
                    }
                    consumer.commitSync();

                }

                //Join all the jobs before picking next batch i.e defined by consumer properties max.poll.records
                /*for(Future f:futures){
                    try{
                        f.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
*/
            }
        }catch (Exception ex){
            log.info("Error handling job.exiting..",ex);
        } finally{
            log.info("Closing kafka consumer for thread :"+Thread.currentThread().getName());
            //consumer.close();
        }
    }

    @AllArgsConstructor
    private static class ConversionJobCallback implements FutureCallback<Map<String, String>> {

        /**
         * Invoked with the result of the {@code Future} computation when it is successful.
         *
         * @param result
         */
        @Override
        public void onSuccess(@Nullable Map<String, String> result) {
            log.info(String.format("Conversion job Success::"));
            result.forEach((k, v) -> {
                System.out.println(k + "::" + v);
            });

        }

        @Override
        public void onFailure(Throwable t) {
            log.error("Conversion job failed.. ", t);
        }
    }

}
