package com.trezi.services;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.trezi.exception.ConverterException;
import com.trezi.utility.Constants;
import com.trezi.utility.FileClientProvider;
import com.trezi.utility.SupportedFileTypes;
import com.trezi.utility.ZipUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * @author Mourya Balla
 * @project converter
 * @CreatedOn 18-06-2019
 */
@Slf4j
public class ConversionServiceAction implements Callable<Map<String, String>> {

    public static ProcessService convertorProcess = new ProcessService();
    SupportedFileTypes fileType=null;
    String sourceFileUrl=null;

    public ConversionServiceAction(SupportedFileTypes fileType, String sourceFileUrl) {
        this.fileType=fileType;
        this.sourceFileUrl=sourceFileUrl;
    }

   /* public static void main(String[] args) throws InvalidKeyException {
        try {
            convertRvtToTZI("https://trezicloudinventory.file.core.windows.net/assetlibrary/test/Conference Table/HMI_Burdick_Group_Oval_Table_36D120W_3ds_3D/HMI_Burdick_Group_Oval_Table_36D120W_3ds_3D.skp");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }*/

    public static boolean convertRvtToTZI(String sourceFileUrl) throws URISyntaxException, StorageException, InvalidKeyException {

        String uploadedFileUrl=null;
        List<File> filesToDelete = new ArrayList<>();

        String[] uploadFilePath=null;
        CloudFileClient fileClient = null;
        URL url = null;
        try {
            String urlStr = URLDecoder.decode(sourceFileUrl, "UTF-8");
            url = new URL(urlStr);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {

            uploadFilePath = url.getPath().split("\\/");
            String[] uploadFilePathArr = Arrays.copyOfRange(uploadFilePath, 2, uploadFilePath.length);
            String path = String.join("/", uploadFilePathArr);

            fileClient = FileClientProvider.getFileClientReference();
            CloudFileShare fileShare = fileClient.getShareReference(uploadFilePath[1]); //File share name

            CloudFileDirectory rootDir = fileShare.getRootDirectoryReference();
            CloudFile cloudFile = rootDir.getFileReference(path);
            String downloadedFilePath = System.getProperty("java.io.tmpdir") + cloudFile.getName();
            File doenloaded = new File(downloadedFilePath);
            new File(doenloaded.getParent()).mkdirs();
            doenloaded.createNewFile();
            log.info(String.format("\tDownload the file from \"%s\" to \"%s\".", cloudFile.getUri().toURL(), downloadedFilePath));
            cloudFile.downloadToFile(downloadedFilePath);
            filesToDelete.add(new File(downloadedFilePath));

            log.debug("DownloadedFile path::"+downloadedFilePath);
            convertorProcess.invokeCommandInWindows(Constants.REVIT_CONVERTER_EXE+ " "   + "\""+doenloaded.getAbsolutePath()+"\"");
            log.debug("Completed Revit process...");

            String[] uploadNameArr = path.split("\\/");
            String uploadName = uploadNameArr[uploadNameArr.length-1];
            int pos = uploadName.lastIndexOf(".");
            String uploadFileName = pos > 0 ? uploadName.substring(0, pos) : uploadName;

            File skpDir = new File(doenloaded.getParent());
            File[] files = skpDir.listFiles((dir1, name) -> name.equals(uploadFileName+".tzi".trim()));
            if (files.length == 0)throw new ConverterException("RVT conversion Failed.", HttpStatus.INTERNAL_SERVER_ERROR);

            ///String zippedTziFile = ZipUtility.zip(files[0]);

            log.info("uploading File..."+files[0].getName());
            filesToDelete.add(files[0]);



            CloudFileDirectory uploadDir = rootDir.getDirectoryReference(path.substring(0,path.lastIndexOf("/")));
            CloudFile upload = uploadDir.getFileReference(uploadFileName+".tzi");
            upload.getProperties().setContentType("application/octet-stream");
            upload.getProperties().setContentDisposition("attachment");
            upload.uploadFromFile(files[0].getAbsolutePath());
            log.info("uploadedFileUrl:" + uploadedFileUrl);


        }catch (IOException ioe){
            log.debug("Exception occurred in conversion service:",ioe);
            return false;
        } finally {
            //deleteDirectory(uploadFileDir);
            //deleteDirectory(new File(System.getProperty("java.io.tmpdir") + uploadFilePath[2]));
            filesToDelete.forEach(File::delete);
        }
        return true;

    }


    public static boolean convertSkpToTZI(String sourceFileUrl) throws URISyntaxException, StorageException {

        String uploadedFileUrl=null;
        String[] uploadFilePath=null;
        CloudFileClient fileClient = null;
        List<File> filesToDelete = new ArrayList<>();
        URL url = null;
        try {
            String urlStr = URLDecoder.decode(sourceFileUrl, "UTF-8");
            url = new URL(urlStr);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {

            uploadFilePath = url.getPath().split("\\/");
            String[] uploadFilePathArr = Arrays.copyOfRange(uploadFilePath, 2, uploadFilePath.length);
            String path = String.join("/", uploadFilePathArr);

            fileClient = FileClientProvider.getFileClientReference();
            CloudFileShare fileShare = fileClient.getShareReference(uploadFilePath[1]); //File share name

            CloudFileDirectory rootDir = fileShare.getRootDirectoryReference();
            CloudFile cloudFile = rootDir.getFileReference(path);
            String downloadedFilePath = System.getProperty("java.io.tmpdir") + cloudFile.getName();
            File doenloaded = new File(downloadedFilePath);
            new File(doenloaded.getParent()).mkdirs();
            doenloaded.createNewFile();

            log.info(String.format("\tDownload the file from \"%s\" to \"%s\".", cloudFile.getUri().toURL(), downloadedFilePath));
            cloudFile.downloadToFile(downloadedFilePath);
            filesToDelete.add(new File(downloadedFilePath));
            //ProcessService processService = null;
            convertorProcess.invokeCommandInWindows(Constants.SKETCH_UP_CONVERTER_EXE + " " /*"D:\\tziTest\\App_ConvertToTZI.exe"+ " "*/ + "\""+downloadedFilePath+"\"");
            //convertorProcess.invokeCommandInWindows("D:\\tziTest\\App_ConvertToTZI.exe" + " " /*"D:\\tziTest\\App_ConvertToTZI.exe"+ " "*/ + "\""+downloadedFilePath+"\"");
            //processService.invokeCommandInWindows(Constants.SKETCH_UP_CONVERTER_EXE + " " + "C:\\Users\\RevitVM0\\Desktop\\SkpToTZIConverter\\Ninety+120x60x30.skp");

            String[] uploadNameArr = path.split("\\/");
            String uploadName = uploadNameArr[uploadNameArr.length-1];
            int pos = uploadName.lastIndexOf(".");
            String uploadFileName = pos > 0 ? uploadName.substring(0, pos) : uploadName;

            File skpDir = new File(doenloaded.getParent());
            File[] files = skpDir.listFiles((dir1, name) -> name.equals(uploadFileName.trim()));
            if (files.length == 0)throw new ConverterException("Skp conversion Failed.", HttpStatus.INTERNAL_SERVER_ERROR);

            String zippedTziFile = ZipUtility.zip(files[0]);

            log.info("uploading File..."+zippedTziFile);
            filesToDelete.add(new File(zippedTziFile));
            FileSystemUtils.deleteRecursively(files[0]);



            CloudFileDirectory uploadDir = rootDir.getDirectoryReference(path.substring(0,path.lastIndexOf("/")));
            CloudFile upload = uploadDir.getFileReference(uploadFileName+".tzi");
            upload.getProperties().setContentType("application/octet-stream");
            upload.getProperties().setContentDisposition("attachment");
            upload.uploadFromFile(zippedTziFile);
            log.info("uploadedFileUrl:" + uploadedFileUrl);

        }catch (Exception ioe){
            log.info("Exception occurred in conversion service:",ioe);
            return false;
        } finally {
            //deleteDirectory(new File(System.getProperty("java.io.tmpdir") + uploadFilePath[2]));
            filesToDelete.forEach(File::delete);
        }
        /*Map<String, Object> response = new HashMap<>();
        response.put("tziFile", uploadedFileUrl);*/
        return true;
    }

    private static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        } // either file or an empty directory
        log.debug("removing file or directory : " + dir.getName());
        return dir.delete();
    }

    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     * @throws Exception if unable to compute a result
     */
    @Override
    public Map<String,String> call() throws Exception {
        log.debug("conversion started..");
        boolean jobStatus=false;
        Map<String,String> result = new HashMap<>();
        switch (fileType){
            case RVT:
                jobStatus = convertRvtToTZI(sourceFileUrl);
                result.put("type",SupportedFileTypes.RVT.toString());
                result.put("url",sourceFileUrl);
                result.put("status",jobStatus?"Success":"Failure");
                break;
            case SKP:
                jobStatus = convertSkpToTZI(sourceFileUrl);
                result.put("type",SupportedFileTypes.SKP.toString());
                result.put("url",sourceFileUrl);
                result.put("status",jobStatus?"Success":"Failure");
                break;
        }
        return result;
    }
}
