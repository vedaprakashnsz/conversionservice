package com.trezi.services;

import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.trezi.exception.ConverterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.sql.SQLOutput;

@Service
public class ProcessService {

    private static final Logger logger = LoggerFactory.getLogger(ProcessService.class.getName());

    @Inject
    BlobService blobService;

    public void invokeCommandInWindows(String command){
        try
        {
            Runtime runtime  = Runtime.getRuntime();
            Process proc = runtime.exec(command);
            while (proc.isAlive()){
                Thread.sleep(5000);
            }
        }
        catch (Exception e)
        {
            System.out.println("Failed to start process fpor skp conversion .... Exiting .... ");
            e.printStackTrace();
        }
    }

    public CloudBlockBlob downloadBlobFromAzure(String path){
        try {
            CloudBlockBlob cloudBlockBlob = blobService.downloadFileToBlob(path);
            return cloudBlockBlob;
        }
        catch (Exception e){
            logger.error(" error while downloading blob " + e.getMessage());
            throw new ConverterException("Error downloading file");
        }
    }

}
