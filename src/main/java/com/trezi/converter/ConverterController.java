package com.trezi.converter;

import com.trezi.services.BlobService;
import com.trezi.services.ProcessService;
import com.trezi.utility.Constants;
import com.trezi.utility.RestResponse;
import com.trezi.utility.ZipUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

@RestController
public class ConverterController {
    private static final Logger logger = LoggerFactory.getLogger(ConverterController.class.getName());

    @Inject
    RestResponse restResponse;

    @Inject
    ProcessService processService;

    @Autowired
    BlobService blobService;

    @RequestMapping(value = "/sample", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> sample() {
        processService.invokeCommandInWindows(Constants.REVIT_INSTALLATION);
        return restResponse.success("Sample");
    }

    @RequestMapping(value = "/convertSkpToTZI", method = RequestMethod.GET/*, consumes = "application/x-www-form-urlencoded"*/)
    @ResponseBody
    public ResponseEntity<?> convertSkpToTZI(@RequestParam("blobLink") String sourceFileUrl){

        List<File> tempFileList = new ArrayList<>();
        String uploadedFileUrl=null;
        File uploadFileDir=null;
        URL url = null;
        try {
            String urlStr = URLDecoder.decode(sourceFileUrl, "UTF-8");
            url = new URL(urlStr);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            File sourceFile = blobService.downloadFileFromContainer(sourceFileUrl, url.getPath());
            System.out.println("\""+sourceFile.getAbsolutePath()+"\"");
            processService.invokeCommandInWindows(Constants.SKETCH_UP_CONVERTER_EXE + " " /*"D:\\tziTest\\App_ConvertToTZI.exe"+ " "*/ + "\""+sourceFile.getAbsolutePath()+"\"");
            //processService.invokeCommandInWindows("D:\\tziTest\\App_ConvertToTZI.exe" + " " /*"D:\\tziTest\\App_ConvertToTZI.exe"+ " "*/ + "\""+sourceFile.getAbsolutePath()+"\"");
            //processService.invokeCommandInWindows(Constants.SKETCH_UP_CONVERTER_EXE + " " + "C:\\Users\\RevitVM0\\Desktop\\SkpToTZIConverter\\Ninety+120x60x30.skp");

            uploadFileDir = sourceFile.getParentFile();

            String[] uploadFilePath = url.getPath().split("\\/");
            String[] uploadFilePathArr = Arrays.copyOfRange(uploadFilePath, 2, uploadFilePath.length);
            String path = String.join("/", uploadFilePathArr);

            int pos = sourceFile.getName().lastIndexOf(".");
            String uploadFileName = pos > 0 ? sourceFile.getName().substring(0, pos) : sourceFile.getName();

            File[] files = uploadFileDir.listFiles((dir, name) -> name.equals(uploadFileName.trim()));
            if (files.length == 0) return restResponse.failureWithCustomCode("Skp conversion Failed.", "500");

            String zippedTziFile = ZipUtility.zip(files[0]);

            logger.info("uploading File...");


            uploadedFileUrl = blobService.uploadFileToContainer(path.split("\\.")[0] + ".tzi", new File(zippedTziFile));

            logger.info("uploadedFileUrl:" + uploadedFileUrl);

        }catch (IOException ioe){
            logger.debug("Exception occurred in conversion service:",ioe);
        } finally {
            deleteDirectory(uploadFileDir);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("tziFile", uploadedFileUrl);
        return restResponse.successWithData(response);
    }

    @RequestMapping(value = "/convertRvtToTZI", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> convertRvtToTZI(@RequestParam("blobLink") String sourceFileUrl) {

        List<File> tempFileList = new ArrayList<>();
        String uploadedFileUrl=null;
        File uploadFileDir =null;
        URL url = null;
        try {
            url = new URL(sourceFileUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            File sourceFile = blobService.downloadFileFromContainer(sourceFileUrl, url.getPath());
            processService.invokeCommandInWindows(Constants.REVIT_CONVERTER_EXE+ " "   + "\""+sourceFile.getAbsolutePath()+"\"");
            uploadFileDir = sourceFile.getParentFile();

            String[] uploadFilePath = url.getPath().split("\\/");
            String[] uploadFilePathArr = Arrays.copyOfRange(uploadFilePath, 2, uploadFilePath.length);
            String path = String.join("/", uploadFilePathArr);

            int pos = sourceFile.getName().lastIndexOf(".");
            String uploadFileName = pos > 0 ? sourceFile.getName().substring(0, pos) : sourceFile.getName();

            File[] files = uploadFileDir.listFiles((dir, name) -> name.equals(uploadFileName + ".tzi"));
            if (files.length == 0)
                return restResponse.failureWithCustomCode("Rvt conversion Failed.", "500");

            //String zippedTziFile = ZipUtility.zip(files[0]);

            uploadedFileUrl = blobService.uploadFileToContainer(path.split("\\.")[0] + ".tzi", files[0]);

            logger.info("uploadedFileUrl:" + uploadedFileUrl);

            //tempFileList.add(sourceFile);
            //Collections.addAll(tempFileList, files);
        }catch (IOException ioe){
            logger.debug("Exception occurred in conversion service:",ioe);
        } finally {
            deleteDirectory(uploadFileDir);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("tziFile", uploadedFileUrl);
        return restResponse.successWithData(response);

    }

    private boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        } // either file or an empty directory
        logger.debug("removing file or directory : " + dir.getName());
        return dir.delete();
    }
}
